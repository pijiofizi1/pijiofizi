<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="icon" sizes="16x16" href="{{ asset('images/icon.ico') }}">
    <title>Sancy101</title>

    <!-- Scripts -->
    <!-- <script src="{{ asset('js/app.js') }}" defer></script> -->

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Avenir" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet'>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('owlcarousel/assets/owl.carousel.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('owlcarousel/assets/owl.theme.default.min.css') }}" />

    <!-- Styles -->
    <!-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> -->
    <style>
        body {
            font-family: 'Avenir', sans-serif !important;
            height: 100%;
        }
        .card {
            border-radius: 10px;
            border: 0px;
        }
    </style>
    @stack('styles')
</head>
<body>
    <nav class="navbar navbar-light bg-light shadow sticky-top">
        <div class="container-fluid">
            <a class="navbar-brand ms-2" href="#"><h2><span style="color:#090C4E;">SANCY</span><span style="color:#F20000;">101</span></h2></a>
            <form class="d-flex">
                <button class="rounded-pill btn btn-outline-primary me-2 px-4" type="button">Log In</button>
                <button class="rounded-pill btn btn-primary px-4" type="button">Sign Up</button>
            </form>
        </div>
    </nav>
    <div>
        @yield('content')
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"></script>
    <script src="https://kit.fontawesome.com/3553276e4a.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script type="text/javascript" src="{{ asset('owlcarousel/owl.carousel.min.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('#page-loader').css('display', 'none');
        });
    </script>
    @stack('scripts')
</body>

</html>
