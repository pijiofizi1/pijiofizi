@extends('layouts.app')
@push('styles')
<style>
    .img-course {
        height: 150px !important;
    }
    small {
        font-size: 11px;
    }
</style>
@endpush
@section('content')
<div class="owl-carousel header-carousel owl-theme">
    <div style="background-image: {{ asset('image2.jpg') }}">
        <div class="position-relative">
            <img src="{{ asset('image2.jpg') }}" alt="">
            <div class="position-absolute" style="top: 25%; left: 5%">
                <div class="card shadow p-3">
                    <div class="card-body">
                        <h2 class="mb-3">Learning that gets you</h2>
                        <h6>Skills for your present (and your future). Get started with us.</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div style="background-image: {{ asset('image1.jpg') }}">
        <div class="position-relative">
            <img src="{{ asset('image1.jpg') }}" alt="">
            <div class="position-absolute" style="top: 25%; left: 5%">
                <div class="card shadow p-3">
                    <div class="card-body">
                        <h2 class="mb-3">Skills that drive you forward</h2>
                        <h6>Technology and the world of work change fast — with us, you’re faster.<br> Get the skills to achieve goals and stay competitive.</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <br>
    <br>
    <h2>Courses to get you started</h2>
    <hr>
    <div class="owl-carousel course-carousel owl-theme">
        <div class="card border">
            <div class="card-body p-0" style="overflow: hidden">
                <img class="img-fluid img-course" src="https://www.sancy101.com/element/IDEAS%20Digitizing%20Medical%20Insurance%20Claims%20:%20Hospital%20Managers-231127093624.png">
            </div>
            <div class="card-footer bg-white">
                <h6 class="fw-bold">@php echo substr('IDEAS Digitizing Medical Insurance Claims : Hospital Managers', 0, 40) . '...'; @endphp</h6>
                <small class="text-muted">Ahmad Hafizi</small>
                <h6 class="fw-bold">RM219.90</h6>
                <div class="d-grid gap-2 col-12 mx-auto">
                    <a class="btn btn-primary" href="{{ url('/payment') }}">Buy This Course</a>
                </div>
            </div>
        </div>
        <div class="card border">
            <div class="card-body p-0" style="overflow: hidden">
                <img class="img-fluid img-course" src="https://www.sancy101.com/element/IDEAS%20Digitizing%20Medical%20Insurance%20Claims%20:%20Doctors-230320040100.png">
            </div>
            <div class="card-footer bg-white">
                <h6 class="fw-bold">@php echo substr('IDEAS Digitizing Medical Insurance Claims : Doctors', 0, 40) . '...'; @endphp</h6>
                <small class="text-muted">Ahmad Hafizi</small>
                <h6 class="fw-bold">RM219.90</h6>
                <div class="d-grid gap-2 col-12 mx-auto">
                    <a class="btn btn-primary" href="{{ url('/payment') }}">Buy This Course</a>
                </div>
            </div>
        </div>
        <div class="card border">
            <div class="card-body p-0" style="overflow: hidden">
                <img class="img-fluid img-course" src="https://www.sancy101.com/element/IDEAS%20Digitizing%20Medical%20Insurance%20Claims%20:%20Hospital%20Officer-230320040043.png">
            </div>
            <div class="card-footer bg-white">
                <h6 class="fw-bold">@php echo substr('IDEAS Digitizing Medical Insurance Claims : Hospital Officer', 0, 40) . '...'; @endphp</h6>
                <small class="text-muted">Ahmad Hafizi</small>
                <h6 class="fw-bold">RM219.90</h6>
                <div class="d-grid gap-2 col-12 mx-auto">
                    <a class="btn btn-primary" href="{{ url('/payment') }}">Buy This Course</a>
                </div>
            </div>
        </div>
        <div class="card border">
            <div class="card-body p-0" style="overflow: hidden">
                <img class="img-fluid img-course" src="https://www.sancy101.com/element/cliniSENSE%20-%20%20Radiology-230327152316.jpg">
            </div>
            <div class="card-footer bg-white">
                <h6 class="fw-bold">cliniSENSE - Laboratory</h6>
                <h6 class="fw-bold mb-0">&nbsp;</h6>
                <small class="text-muted">Ahmad Hafizi</small>
                <h6 class="fw-bold">RM219.90</h6>
                <div class="d-grid gap-2 col-12 mx-auto">
                    <a class="btn btn-primary" href="{{ url('/payment') }}">Buy This Course</a>
                </div>
            </div>
        </div>
        <div class="card border">
            <div class="card-body p-0" style="overflow: hidden">
                <img class="img-fluid img-course" src="https://www.sancy101.com/element/IDEAS%20Digitizing%20Medical%20Insurance%20Claims%20:%20Hospital%20Managers-231127093624.png">
            </div>
            <div class="card-footer bg-white">
                <h6 class="fw-bold">@php echo substr('IDEAS Digitizing Medical Insurance Claims : Hospital Managers', 0, 40) . '...'; @endphp</h6>
                <small class="text-muted">Ahmad Hafizi</small>
                <h6 class="fw-bold">RM219.90</h6>
                <div class="d-grid gap-2 col-12 mx-auto">
                    <a class="btn btn-primary" href="{{ url('/payment') }}">Buy This Course</a>
                </div>
            </div>
        </div>
        <div class="card border">
            <div class="card-body p-0" style="overflow: hidden">
                <img class="img-fluid img-course" src="https://www.sancy101.com/element/IDEAS%20Digitizing%20Medical%20Insurance%20Claims%20:%20Doctors-230320040100.png">
            </div>
            <div class="card-footer bg-white">
                <h6 class="fw-bold">@php echo substr('IDEAS Digitizing Medical Insurance Claims : Doctors', 0, 40) . '...'; @endphp</h6>
                <small class="text-muted">Ahmad Hafizi</small>
                <h6 class="fw-bold">RM219.90</h6>
                <div class="d-grid gap-2 col-12 mx-auto">
                    <a class="btn btn-primary" href="{{ url('/payment') }}">Buy This Course</a>
                </div>
            </div>
        </div>
        <div class="card border">
            <div class="card-body p-0" style="overflow: hidden">
                <img class="img-fluid img-course" src="https://www.sancy101.com/element/IDEAS%20Digitizing%20Medical%20Insurance%20Claims%20:%20Hospital%20Officer-230320040043.png">
            </div>
            <div class="card-footer bg-white">
                <h6 class="fw-bold">@php echo substr('IDEAS Digitizing Medical Insurance Claims : Hospital Officer', 0, 40) . '...'; @endphp</h6>
                <small class="text-muted">Ahmad Hafizi</small>
                <h6 class="fw-bold">RM219.90</h6>
                <div class="d-grid gap-2 col-12 mx-auto">
                    <a class="btn btn-primary" href="{{ url('/payment') }}">Buy This Course</a>
                </div>
            </div>
        </div>
        <div class="card border">
            <div class="card-body p-0" style="overflow: hidden">
                <img class="img-fluid img-course" src="https://www.sancy101.com/element/cliniSENSE%20-%20%20Radiology-230327152316.jpg">
            </div>
            <div class="card-footer bg-white">
                <h6 class="fw-bold">cliniSENSE - Laboratory</h6>
                <h6 class="fw-bold mb-0">&nbsp;</h6>
                <small class="text-muted">Ahmad Hafizi</small>
                <h6 class="fw-bold">RM219.90</h6>
                <div class="d-grid gap-2 col-12 mx-auto">
                    <a class="btn btn-primary" href="{{ url('/payment') }}">Buy This Course</a>
                </div>
            </div>
        </div>
    </div>

    <br>
    <h2>List of categories</h2>
    <hr>
    <div class="owl-carousel course-carousel owl-theme">
        <div class="card border">
            <div class="card-body p-0" style="overflow: hidden;">
                <img class="img-fluid" style="height:200px" src="https://www.sancy101.com/element/group_image/group-IDEAS-230906090646.png">
            </div>
            <div class="card-footer bg-white text-center">
                <h6>IDEAS</h6>
            </div>
        </div>
        <div class="card border">
            <div class="card-body p-0" style="overflow: hidden;">
                <img class="img-fluid" style="height:200px" src="https://www.sancy101.com/element/group_image/group-cliniSENSE-230906090338.png">
            </div>
            <div class="card-footer bg-white text-center">
                <h6>cliniSense</h6>
            </div>
        </div>
        <div class="card border">
            <div class="card-body p-0" style="overflow: hidden;">
                <img class="img-fluid" style="height:200px" src="https://www.sancy101.com/element/group_image/group-Micro-Learning-230906090914.png">
            </div>
            <div class="card-footer bg-white text-center">
                <h6>Micro-Learning</h6>
            </div>
        </div>
        <div class="card border">
            <div class="card-body p-0" style="overflow: hidden;">
                <img class="img-fluid" style="height:200px" src="https://www.sancy101.com/element/group_image/group-IDEAS%20(Thai)-231122041755.png">
            </div>
            <div class="card-footer bg-white text-center">
                <h6>IDEAS (Thai)</h6>
            </div>
        </div>
        <div class="card border">
            <div class="card-body p-0" style="overflow: hidden;">
                <img class="img-fluid" style="height:200px" src="https://www.sancy101.com/element/group_image/group-icuCare-231129035834.jpg">
            </div>
            <div class="card-footer bg-white text-center">
                <h6>icuCare</h6>
            </div>
        </div>
    </div>
</div>

<br>
<br>
@endsection

@push('scripts')
<script>
    $('.header-carousel').owlCarousel({
        margin: 10,
        autoplay: true,
        autoplaySpeed: 1000,
        loop: true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
            },
            600: {
                items: 1,
            },
            1000: {
                items: 1,
            }
        }
    });
    $('.course-carousel').owlCarousel({
        margin: 10,
        autoplay: true,
        autoplaySpeed: 1000,
        loop: true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 2,
            },
            600: {
                items: 2,
            },
            1000: {
                items: 5,
            }
        }
    });
    $('.categories-carousel').owlCarousel({
        margin: 10,
        autoplay: true,
        autoplaySpeed: 1000,
        loop: true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 2,
            },
            600: {
                items: 2,
            },
            1000: {
                items: 5,
            }
        }
    });
</script>
@endpush
