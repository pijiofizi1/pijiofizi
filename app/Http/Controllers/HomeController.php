<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Stripe\StripeClient;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function payment(Request $request)
    {
        $stripe               = new \Stripe\StripeClient('sk_test_51NKw8KAH9JCeEjoym7r1ZAls7xghhQKUMF3mh84ClbLvIWnYuMFnu4e3rgmdISyHzasL7NmwMfxnbnZTdgu0U7Mv001i7182O0');
        $payment_method_types = ['card'];

        $product = $stripe->products->create([
            'name'        => 'IDEAS Digitizing Medical Insurance Claims',
            'description' => 'Purchasing Course',
        ]);
        $price = $stripe->prices->create([
            'unit_amount' => 219.90 * 100,
            'currency'    => 'USD',
            'product'     => $product->id,
        ]);
        $session = $stripe->checkout->sessions->create([
            'client_reference_id'        => 'testingforsancy',
            'mode'                       => 'payment',
            'line_items'                 => [
                ['price'              => $price->id,
                    'quantity'            => 1,
                    'adjustable_quantity' => [
                        'enabled' => true,
                    ],
                ],
            ],
            'phone_number_collection'    => ['enabled' => 'true'],
            'payment_method_types'       => $payment_method_types,
            'billing_address_collection' => 'required',
            'success_url'                => url('/'),
            'cancel_url'                 => url('/'),
        ]);
        return redirect($session->url);
    }
}
