<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name'              => 'Ahmad Hafizi',
            'email'             => 'pijiofizi@gmail.com',
            'email_verified_at' => now(),
            'password'          => Hash::make('pijiofizi@gmail.com'),
            'remember_token'    => Str::random(10),
        ]);
        $user->assignRole('admin');
    }
}
